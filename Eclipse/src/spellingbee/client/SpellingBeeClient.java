/**
 * SpellingBeeClient Creates a GameTab and a ScoreTab and 
 * adds them to the gui for a spelling bee game to begin.
 * 
 * @author Aharon Moryoussef/Shlomo Bensimhon
 * @since	2020-12-01
 *
 */
package spellingbee.client;
import spellingbee.network.*;
import spellingbee.game.*;
import java.util.*;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
/**
 * SpellingBeeClient 
 * 
 * @author Aharon Moryoussef/Shlomo Bensimhon
 * @since	2020-12-01
 *
 * SpellingBeeClient is an object that extends from the application class
 * and is responsible for running the game
 */


public class SpellingBeeClient extends Application{
	
	/**
	 * The start method is what brings the whole game together.
	 * The game section and the ranking system is separated by there own respective tab
	 * @param Stage stage
	 */
	public void start(Stage stage) {
		
		Client client = new Client();
		Group root = new Group();
		
		TabPane mainPane = new TabPane();
		
		GameTab game = new GameTab(client);
		mainPane.getTabs().add(game);
		
		ScoreTab score = new ScoreTab(client);
		mainPane.getTabs().add(score);
		
		root.getChildren().add(mainPane);	

		
		TextField field = game.getScoreField();
		
		field.textProperty().addListener((observable, oldValue, newValue) -> {
			score.refresh();
		});

		
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.OLDLACE);

		//associate scene to stage and show
		stage.setTitle("Spelling Bee"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	/**
	 * This main method is what will run the application 
	 * by calling Launch(args).
	 */
  public static void main(String[] args) {
      Application.launch(args);
  }
}    
