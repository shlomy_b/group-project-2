package spellingbee.network;

import spellingbee.game.*;
import spellingbee.client.*;
import java.util.*;

/**
 * This class will run on the server side and is used to connect the server code to the backend business code.
 * This class is where the "protocol" will be defined.
 */
public class ServerController {
	// This is the interface you will be creating!
	// Since we are storing the object as an interface, you can use any type of ISpellingBeeGame object.
	// This means you can work with either the SpellingBeeGame OR SimpleSpellingBeeGame objects and can
	// seamlessly change between the two.
	
	//private ISpellingBeeGame spellingBee = new SimpleSpellingBeeGame("hello");
	
	private SpellingBeeGame game1 = new SpellingBeeGame();
	
	/**
	 * Action is the method where the protocol translation takes place.
	 * This method is called every single time that the client sends a request to the server.
	 * It takes as input a String which is the request coming from the client. 
	 * It then does some actions on the server (using the ISpellingBeeGame object)
	 * and returns a String representing the message to send to the client
	 * @param inputLine The String from the client
	 * @return The String to return to the client
	 */
	public String action(String inputLine) {
		/* Your code goes here!!!!
		 * Note: If you want to preserve information between method calls, then you MUST
		 * store it into private fields.
		 * You should use the spellingBee object to make various calls and here is where your communication
		 * code/protocol should go.
		 * For example, based on the samples in the assignment:
		 * */
		 if (inputLine.equals("getCenterLetter")) {
		  	// client has requested getCenter. Call the getCenter method which returns a String of 7 letters
			 //return spellingBee.getAllLetters();
			 //System.out.println("It Works!!!!!!");
			 return String.valueOf(this.game1.getCenterLetter());
		  }
		 if (inputLine.equals("getScore")) {
			 return this.game1.getScore() + "";
		 }
		 
		 if (inputLine.equals("getBrackets")) {
			 //return Arrays.toString(this.game1.getBrackets());
			 String arrayString = Arrays.toString(this.game1.getBrackets());
			 //System.out.println(Arrays.toString(this.game1.getBrackets()));
			 //System.out.println(arrayString.substring(1, arrayString.length() - 1).replaceAll(" ", ""));
			 return arrayString.substring(1, arrayString.length() - 1).replaceAll(" ", "");
		 }
		 if (inputLine.equals("getLetters")) {
			 return this.game1.getAllLetters();
		 }
		 
		 if(!(inputLine.equals(""))) { //verifying if the word is good or not does not work			 
			 String reply = "";
			 if(!(this.game1.getWordsFound().contains(inputLine))) {
				  reply += this.game1.getMessage(inputLine);
				  reply += ",";
				  reply += String.valueOf(this.game1.getPointsForWord(inputLine));
				  
				  this.game1.addPoints(this.game1.getPointsForWord(inputLine));
			 }else {
				 reply = "Word Already Entered";
			 }
			return reply;
		 }
		return ""; 
	} 
}
