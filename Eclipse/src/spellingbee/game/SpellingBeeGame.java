/**
 * SpellingBeeGame is a object that incorporates all the
 * methods and attributes needed to run a spelling be gam.
 * 
 * @author Shlomo Bensimhon
 * @since	2020-12-01
 *
 */

package spellingbee.game;
import java.io.IOException;
import java.nio.*;
import java.nio.file.*;
import java.util.*;

public class SpellingBeeGame implements ISpellingBeeGame{
	private String wordSet;
	private String middleLetter;
	private int currentScore;
	private ArrayList<String> wordsFound;   // null at start, then at the end, we will fill this once the user guesses words
	private static String[] possibleWords;
	
	public SpellingBeeGame() {
		try {
		String[] wordSets = createWordsFromFile("letterCombinations.txt");
		Random r1 = new Random(); 
		final int randomIndex = r1.nextInt(500); //there's 500 lines in letterCombinations
		final int middleLetterIndex = r1.nextInt(7);
		String randomWordSet = wordSets[randomIndex];
		char tempMiddleLetter = randomWordSet.charAt(middleLetterIndex);
		
		this.middleLetter = Character.toString(tempMiddleLetter);
		this.wordSet = randomWordSet;
		this.currentScore = 0;
		this.wordsFound = new ArrayList<String>();
		this.possibleWords = createWordsFromFile("english.txt");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public SpellingBeeGame(String word) {
		try {
		Random r1 = new Random(); 
		int middleLetterIndex = r1.nextInt(7);
		char tempMiddleLetter = word.charAt(middleLetterIndex);
		
		this.middleLetter = Character.toString(tempMiddleLetter);
		this.wordSet = word;
		this.currentScore = 0;
		this.wordsFound = new ArrayList<String>();
		this.possibleWords = createWordsFromFile("english.txt");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	* createWordsFromFile takes as input a name of a text file and returns
	* a String array filled with each line of that text file.
	*
	* @param  path	The name of the file you are looking to extract from. 
	* @return       An Array containing all the lines from said file.
	*/
	public String[] createWordsFromFile(String path) throws IOException {
		Path p = Paths.get(path);
		List<String> lines = Files.readAllLines(p);
		String[] returnArray = new String[lines.size()];
		returnArray = lines.toArray(returnArray);
		return returnArray;
	}
	
	/**
	* getBrackets calculates the maximum number of points a player can 
	* receive with a set of letters. Then the method returns an array
	* with 5 positions. The total Point *.25, *.50, *.75, *.90, *.1
	* are the five elements that are returned
	*
	* @return       An Array containing all the lines from said file.
	*/
	public int[] getBrackets() { 
		int totalPoints = 0;
		ArrayList<String> tempPossibleWords = findPossibleWords();
		for (int i = 0; i < tempPossibleWords.size(); i++) {
			if(allLetters(tempPossibleWords.get(i))) {
				totalPoints += (tempPossibleWords.get(i).length() + 7);
			}else if(tempPossibleWords.get(i).length() == 4){
				totalPoints += 1;
			}else {
				totalPoints += tempPossibleWords.get(i).length();
			}
		}
		int[] returnArray = new int[5];
			returnArray[0] = (int)(totalPoints * 0.25);
			returnArray[1] = (int)(totalPoints * 0.50);
			returnArray[2] = (int)(totalPoints * 0.75);
			returnArray[3] = (int)(totalPoints * 0.90);
			returnArray[4] = (totalPoints * 1);
		
		return returnArray;
	}
	
	/**
	* findPossibleWords finds all the possible words that can be guessed 
	* using a set of letters and returns them in the form of an ArrayList.
	*
	* @return       An ArrayList containing all the Possible Words.
	*/
	public ArrayList<String> findPossibleWords(){
		ArrayList<String> tempPossibleWords = new ArrayList<String>();
		for(int i = 0; i < possibleWords.length; i++) {
			int counter = 0;
			if(possibleWords[i].contains(this.middleLetter) && possibleWords[i].length() >= 4) { //here
				for(int j = 0; j < possibleWords[i].length(); j++) {
					if(this.wordSet.contains(String.valueOf(possibleWords[i].charAt(j)))) {
						counter++;
					}
				}
			}
			if(counter == possibleWords[i].length()) {
				tempPossibleWords.add(possibleWords[i]);
			}
		}
		return tempPossibleWords;
	}
	
	/**
	* getPointsForWord takes as input a word and return the amount 
	* of points that that word is worth.
	* 
	* 
	* @param  attempt	The Word you are getting getting points for. 
	* @return       The amount of points the word is worth.
	*/
	public int getPointsForWord(String attempt) {
		ArrayList<String> tempPossibleWords = findPossibleWords();
		if(tempPossibleWords.contains(attempt)) {
		
		if(attempt.length() < 4) {
			return 0;
		}else if(allLetters(attempt) && attempt.length() == 7) {
			return (attempt.length() + 7);
		}else if(attempt.length() == 4) {
			return 1;
		}else if(attempt.length() > 4) {
			return attempt.length();
			}
		}
		return 0;
	}
	
	/**
	* getMessage takes as input a string and returns a message detailing 
	* the validity of the String. ex too short, word doesnt exit...
	* 
	* 
	* @param  attempt	The Word you are getting getting a message for. 
	* @return       The message regarding the word.
	*/
	public String getMessage(String attempt) {
		ArrayList<String> tempPossibleWords = findPossibleWords();
		
		for(int i = 0; i < attempt.length(); i++) {
			if(!(this.wordSet.contains(String.valueOf(attempt.charAt(i))))) {
				return "Unsupported Letter Used";
			}
		}
		
		if(!(attempt.contains(this.middleLetter))) {////here
			return "Missing Middle Letter";
		}else if(attempt.length() < 4) {
			return "Word Entered is Too Short";
		}else if(attempt.length() == 4 && tempPossibleWords.contains(attempt)) {
			this.wordsFound.add(attempt);
			return "Good!";
		}else if(attempt.length() == 5 && tempPossibleWords.contains(attempt)) {
			this.wordsFound.add(attempt);
			return "Great!";
		}else if(attempt.length() == 6 && tempPossibleWords.contains(attempt)) {
			this.wordsFound.add(attempt);
			return "Excellent!";
		}else if(attempt.length() >= 7 && tempPossibleWords.contains(attempt)) {
			this.wordsFound.add(attempt);
			return "Legendary!";
		}else {
			return "Word Does Not Exist";
		}
	}		
	
	/**
	* getAllLetters returns the this.wordSet field 
	* 
	* @return       this.wordSet 
	*/
	public String getAllLetters() {
		return this.wordSet;
	}
	
	/**
	* getCenterLetter returns the this.middleLetter field 
	* 
	* @return       this.middleLetter
	*/
	public char getCenterLetter() {
		return this.middleLetter.charAt(0);
	}
	
	/**
	* getScore returns the this.currentScore field 
	* 
	* @return      this.currentScore
	*/
	public int getScore() { 
		return this.currentScore;
	}
	
	/**
	* addPoints adds a score to the this.currentScore field.
	* 
	* @param  score The Score that will be added to the total score. 
	*/
	public void addPoints(int score) {
		this.currentScore += score;
	}
	
	/**
	* getWordsFound returns ArrayList<String> this.wordsFound
	* 
	* @return      ArrayList<String> this.wordsFound
	*/
	public ArrayList<String> getWordsFound() {
		return this.wordsFound;
	}
	
	/**
	* allLetters is a helper method that checks to see if a word
	* contains all the letters in this.wordSet. If it does, it returns 
	* true, if not it returns false.
	* 
	* @param  word The Score that will be compared. 
	* @return      true or false depending on if the word contains
	*/
	public boolean allLetters(String word) {
		String[] setLetters = this.wordSet.split("");
		for(String c : setLetters) {
			if(!(word.contains(c))) {
				return false;
			}
		}
		return true;
	}
}














