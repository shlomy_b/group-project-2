package spellingbee.game;
/**
 * @author Aharon Moryoussef
 * 
 * @since 2020-12-11
 * 
 * This class is a simplified version of the spelling bee game and is used just
 * to test the game at an early stage by being able to hardcode certain values
 */
public class SimpleSpellingBeeGame implements ISpellingBeeGame {
		private int pointsForWord;
		private String message;
		private String allLetters;
		private char centerLetter;
		private int score;
		private int[] bracket;
		
	
	/**
	 * SimpleSpellingBeeGame is a constructor that takes in a word
	 * and initializes 4 attributes by using that word. 
	 * @param word
	 */
	public SimpleSpellingBeeGame(String word) {
		this.pointsForWord = getPointsForWord(word);
		this.message = getMessage(word);
		this.allLetters = getAllLetters();
		this.centerLetter = getCenterLetter();
	}
	
	@Override
	/**
	 * getPointsForWord is a method in which returns an int
	 * that represents the value of a given word according to the spelling bee rules
	 */
	public int getPointsForWord(String attempt) {

		if(attempt.length() == 4) {
			this.pointsForWord = 1;
			this.score += 1;
			return this.pointsForWord;
		}else if(attempt.length()>4) {
			this.pointsForWord = attempt.length();
			this.score += attempt.length();
			return this.pointsForWord;
		}else if(attempt.contains(getAllLetters())) {
			this.pointsForWord = (attempt.length() + 7);
			this.score += attempt.length() + 7;
			return this.pointsForWord;
		}else {
			return this.pointsForWord; // For now, if any attempt does not match the if statement
						// return 0 points;
		}
		
	}
	
	/**
	 * getMessage is a method in which returns a String
	 * representing a response to the user's guess.
	 *
	 */
	@Override
	public String getMessage(String attempt) {
		// TODO Auto-generated method stub
		boolean containsMiddleLetter = false;
		for(int i = 0; i < attempt.length(); i++) {
			if(attempt.charAt(i) == getCenterLetter()) {
				containsMiddleLetter = true;
			}
		}
		if(attempt.length() < 4) {
			this.message = "Too short!";
		}else if(containsMiddleLetter == false) {
			this.message ="Missing center letter";
		}else if( attempt.length() == 4) {
			this.message = "Good!";
		}else if(attempt.length() == 5) {
			this.message = "Great!";
		}else if(attempt.length() == 6) {
			this.message = "Excellent!";
		}else if(attempt.length() >= 7) {
			this.message = "Legendary!";
		}
		return this.message;
		
	}
	
	/**
	 * This returns a string which represents 
	 * all the letters that the user 
	 * is allowed to use 
	 * in his attempt
	 */
	@Override
	public String getAllLetters() {
		// TODO Auto-generated method stub
		return "ailhvye";
	}

	@Override
	/**
	 * getCenterLetter returns a Char which represents the letter
	 * that is obligated to be included in the attempt the user makes 
	 */
	public char getCenterLetter() {
		// TODO Auto-generated method stub
		return 'e';
	}
	
	/**
	 * This method returns an int that represents the user's score.
	 */
	@Override
	public int getScore() {
		// TODO Auto-generated method stub
		
		
		return this.score;
	}

	@Override
	/**
	 * This method returns an int array which will 
	 * tell the user in which ranking does he belong in, based off his points
	 */
	public int[] getBrackets() {
		// TODO Auto-generated method stub
		int [] ranking = {5,10,15,20,25};
		
		
		return ranking;
	}

}
