package spellingbee.game;

import spellingbee.*;
import spellingbee.network.Client;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
/**
 * @author Aharon Moryoussef
 * 
 * @since 2020-12-11
 * 
 * GameTab is a class that extends the Tab class and will hold the spelling bee game inside of it
 * 
 */

public class GameTab extends Tab implements EventHandler<ActionEvent> {
	private Client c;
	private Button one;
	private Button two;
	private Button three;
	private Button four;
	private Button five;
	private Button six;
	private Button seven;
	private Button delete;
	private Button clear;
	private Button submit;
	private String allLetters;
	private	String center;
	private TextField input;
	private TextField message;
	private TextField points;
	
	public GameTab(Client c) {
		super("Game");
		this.c = c;
		open();
	}
/**
 * open() will be the content displayed in the gameTab. 
 * It adds eventListeners to every button(letter,submit,clear,delete)
 * The results will then be displayed according to what the server responds in 
 * respective text fields: 1) message, 2)points
 */
	public void open() {
		
		Group root = new Group();	
		VBox overall = new VBox();
		HBox buttons = new HBox();
		HBox textFields = new HBox();
		HBox options = new HBox();
		HBox results = new HBox();
		
		this.allLetters = this.c.sendAndWaitMessage("getLetters");
		this.center = this.c.sendAndWaitMessage("getCenterLetter");
		
		 one = new Button(String.valueOf(allLetters.charAt(0)));
		 two = new Button(String.valueOf(allLetters.charAt(1)));
		 three = new Button(String.valueOf(allLetters.charAt(2)));
		 four = new Button(String.valueOf(allLetters.charAt(3)));
		 five = new Button(String.valueOf(allLetters.charAt(4)));
		 six = new Button(String.valueOf(allLetters.charAt(5)));
		 seven = new Button(String.valueOf(allLetters.charAt(6)));
		
		
			if(String.valueOf(allLetters.charAt(0)).equals(center)) {
				one.setTextFill(Color.RED);
			}else if(String.valueOf(allLetters.charAt(1)).equals(center)) {
				two.setTextFill(Color.RED);
			}else if(String.valueOf(allLetters.charAt(2)).equals(center)) {
				three.setTextFill(Color.RED);
			}else if(String.valueOf(allLetters.charAt(3)).equals(center)) {
				four.setTextFill(Color.RED);
			}else if(String.valueOf(allLetters.charAt(4)).equals(center)) {
				five.setTextFill(Color.RED);
			}else if(String.valueOf(allLetters.charAt(5)).equals(center)) {
				six.setTextFill(Color.RED);
			}else if(String.valueOf(allLetters.charAt(6)).equals(center)) {
				seven.setTextFill(Color.RED);
			}
		
		
		
		buttons.getChildren().addAll(one,two,three,four,five,six,seven);
		
		input = new TextField();
		input.setPrefWidth(500);
		
		textFields.getChildren().addAll(input);
		
		 this.submit = new Button("Submit");
		 this.clear = new Button("Clear");
		 this.delete = new Button ("Delete");
		
		options.getChildren().addAll(submit,clear,delete);
		
		message = new TextField(); //This is the message field
		message.setPrefWidth(250);
		
		points = new TextField();//this is the points field
		points.setPrefWidth(250);
		
		results.getChildren().addAll(message,points);
		
		submit.setTextFill(Color.MEDIUMPURPLE);
		submit.setStyle("-fx-font-weigth: bold");
		
		
		one.setOnAction(this);
		two.setOnAction(this);
		three.setOnAction(this);
		four.setOnAction(this);
		five.setOnAction(this);
		six.setOnAction(this);
		seven.setOnAction(this);
		submit.setOnAction(this);
		clear.setOnAction(this);
		delete.setOnAction(this);
		
		clear.setTextFill(Color.DARKRED);
		
		delete.setTextFill(Color.SALMON);
		overall.getChildren().addAll(buttons,textFields,options,results);
		overall.setStyle("-fx-font-weigth: bold");
		root.getChildren().addAll(overall);
		
		this.setContent(root);
	}
/**
 * This method is used whenever a button is clicked 
 * it handles the event that occurs, hence this is why it is named the 
 * "handle" method
 */
@Override
public void handle(ActionEvent event) {
	// TODO Auto-generated method stub
	if(event.getSource() == one) {
		this.input.appendText(String.valueOf(this.allLetters.charAt(0)));
	}else if(event.getSource() == two) {
		this.input.appendText(String.valueOf(this.allLetters.charAt(1)));
	}else if(event.getSource() == three) {
		this.input.appendText(String.valueOf(this.allLetters.charAt(2)));
	}else if(event.getSource() == four) {
		this.input.appendText(String.valueOf(this.allLetters.charAt(3)));
	}else if(event.getSource() == five) {
		this.input.appendText(String.valueOf(this.allLetters.charAt(4)));
	}else if(event.getSource() == six) {
		this.input.appendText(String.valueOf(this.allLetters.charAt(5)));
	}else if(event.getSource() == seven) {
		this.input.appendText(String.valueOf(this.allLetters.charAt(6)));
	}else if(event.getSource() == delete) {
		if(this.input.getLength() > 0) {
		String updateInput = this.input.getText().substring(0,this.input.getLength() - 1);
		this.input.setText(updateInput);
		}else {
			this.input.setText("");
		}
	}else if(event.getSource() == clear) {
		this.input.setText("");
	}else if(event.getSource() == submit) {
		String response = c.sendAndWaitMessage(input.getText());
		String [] seperatedResponse = (response.split(","));
		
		String CurrentScore = c.sendAndWaitMessage("getScore");
	
		message.setText(seperatedResponse[0]);
		message.setEditable(false);
		points.setText(CurrentScore);
		points.setEditable(false);
	}
}

	/**
	 * This method will return the textField that displays the number of points.
	 */
	public TextField getScoreField() {
		return points;
	}
}
