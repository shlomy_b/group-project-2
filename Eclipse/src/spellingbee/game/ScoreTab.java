/**
 * ScoreTab is a class that creates and populates a tab object 
 * that gets returned to the spellingbeeclient
 * 
 * 
 * @author Shlomo Bensimhon
 * @since	2020-12-01
 *
 */

package spellingbee.game;
import spellingbee.network.*;
import spellingbee.client.*;
import java.util.*;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class ScoreTab extends Tab {
	private Client client;
	
	public ScoreTab(Client client) {
		super("Score");
		this.client = client;
		String scoreStrings = this.client.sendAndWaitMessage("getBrackets");
		int currentScore = Integer.parseInt(this.client.sendAndWaitMessage("getScore"));
		setStage(currentScore, scoreStrings);
	}
	
	/**
	* setStage Creates and populates the score tab that is sent to
	* spellingbeecleint using this.setContent()
	*
	* @param  currentScore	The Players current Score. 
	* @param  brackets	The brackets of the possible points he can get. 
	*/
	public void setStage(int currentScore, String brackets) {
		String[] scoreStrings = brackets.split(",");
		GridPane grid1 = new GridPane();
		
		Text tf1 = new Text("Legendary!");
		Text tf2 = new Text("Excellent!");
		Text tf3 = new Text("Great!");
		Text tf4 = new Text("Good!");
		Text tf5 = new Text("Getting Started");
		Text tf6 = new Text("Current Score");
		
		Text score1 = new Text(scoreStrings[4] + ""); 
		Text score2 = new Text(scoreStrings[3] + "");
		Text score3 = new Text(scoreStrings[2] + "");
		Text score4 = new Text(scoreStrings[1] + "");
		Text score5 = new Text(scoreStrings[0] + "");
		Text currentScoreTab = new Text(currentScore + "");
		
		//Button one = new Button("Press Me");
		//grid1.add(one, 0, 10);
		//one.setOnAction(buttonHandler);
	
		if(currentScore > Integer.parseInt(scoreStrings[4])) {	
			tf5.setFill(Color.GREY);
			tf4.setFill(Color.GREY);
			tf3.setFill(Color.GREY);
			tf2.setFill(Color.GREY);
			tf1.setFill(Color.GREY);
		}else if(currentScore > Integer.parseInt(scoreStrings[3])) {	
			tf4.setFill(Color.GREY);
			tf3.setFill(Color.GREY);
			tf2.setFill(Color.GREY);
			tf1.setFill(Color.GREY);
		}else if(currentScore > Integer.parseInt(scoreStrings[2])) {
			tf3.setFill(Color.GREY);
			tf2.setFill(Color.GREY);
			tf1.setFill(Color.GREY);
		}else if(currentScore > Integer.parseInt(scoreStrings[1])) {
			tf2.setFill(Color.GREY);
			tf1.setFill(Color.GREY);
		}else if(currentScore > Integer.parseInt(scoreStrings[0])){
			tf1.setFill(Color.GREY);
		}
		
		grid1.add(tf1, 0, 0);
		grid1.add(tf2, 0, 1);
		grid1.add(tf3, 0, 2);
		grid1.add(tf4, 0, 3);
		grid1.add(tf5, 0, 4);
		grid1.add(tf6, 0, 5);
		
		grid1.add(score1, 1, 0);
		grid1.add(score2, 1, 1);
		grid1.add(score3, 1, 2);
		grid1.add(score4, 1, 3);
		grid1.add(score5, 1, 4);
		grid1.add(currentScoreTab, 1, 5);
		
		grid1.setHgap(10);
		grid1.setVgap(5);
		grid1.setStyle("-fx-font: 20 arial;");
		
		this.setContent(grid1);
	}
	
	//EventHandler<ActionEvent> buttonHandler = new EventHandler<ActionEvent>() {
		//Client client = new Client();

	   // public void handle(ActionEvent event) {
	    	//System.out.println("It Works!!!!!!");
	    	//System.out.println(this.client.sendAndWaitMessage("getCenter"));
	    //}
	//};
	
	/**
	* refresh calls the setStage method in order to set the stage 
	* with an updated currentScore value
	*/
	public void refresh() {
		setStage(Integer.parseInt(this.client.sendAndWaitMessage("getScore")), this.client.sendAndWaitMessage("getBrackets"));
	}
}





