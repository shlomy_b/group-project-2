package spellingbee.game;

import spellingbee.*;
import spellingbee.network.Client;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class TestSpellingBee extends Application{

	public static void main(String[] args) {
		Application.launch(args);

	}

	@Override
public void start(Stage stage) {
		SimpleSpellingBeeGame ssbg = new SimpleSpellingBeeGame("hello");

		
		TabPane tabPane = new TabPane();
		Tab game = new Tab();
		game.setText("Game");
		
		Tab score = new Tab();
		score.setText("Score");
	
		tabPane.getTabs().addAll(game,score);
		//new GameTab().open(stage); //Stuck here, need to implement the game tab inside
		AnchorPane pane = new AnchorPane();
		AnchorPane.setTopAnchor(tabPane, 15.0);
	    AnchorPane.setRightAnchor(tabPane, 15.0);
	    AnchorPane.setBottomAnchor(tabPane, 15.0);
	    AnchorPane.setLeftAnchor(tabPane, 15.0);
		pane.getChildren().addAll(tabPane);
		
		Scene scene = new Scene(pane,800,500);
		scene.setFill(Color.ALICEBLUE);
		
		stage.setTitle("Simple SpellingBee Game");
		stage.setScene(scene);
	
		pane.getChildren().addAll();
		
		stage.setScene(scene);
		stage.show();
		
		
	}
}
