/**
 * tester XXXXXXXXXXXX
 * 
 * @author Shlomo Bensimhon
 * @since	2020-12-01
 *
 */

package spellingbee.game;

import java.io.IOException;
import java.nio.*;
import java.nio.file.*;
import java.util.*;
import spellingbee.*;
import spellingbee.network.Client;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class tester {
	public static void main(String[] args) throws IOException{

		Path p = Paths.get("letterCombinations.txt");
		List<String> lines = Files.readAllLines(p);
		
		
		String[] possibleWords = createWordsFromFile("english.txt");
		
		ArrayList<String> tempSet = findPossibleWords(possibleWords,  "shlomo", "o");
		
		String attempt = "lcottery";
		String wordSet = "oltetry";
		System.out.println(wordSet.contains(attempt));
		
		
		//for(int i = 0; i < attempt.length(); i++) {
			//if(!(wordSet.contains(String.valueOf(attempt.charAt(i))))) {
				//System.out.println("User Entered A Unsupported Letter");
			//}
		//}
		
		
		for (String s : tempSet) {
			System.out.println(s);
		}
		
		
		
		
		
		//int counter = 0;
		//for (String s : tempSet) {
			//System.out.println(s);
			//counter++;
		//}
		//System.out.println(counter);
		
		// total number of words in english.txt = 234936
		
		//String[] possibleWords = {"shlomo" , "shlo", "homo"};
		
		//ArrayList<String> tempSet = findPossibleWords(possibleWords,  "shlomo", "o");
		//int[] brackets = getBrackets(tempSet);
		
		//int counter = 0;
		//for (int i = 0; i < brackets.length; i++) {
			//if(tempSet[i]) {
			//counter++;
			//System.out.println(brackets[i]);
			//}
		 //String arrayString = Arrays.toString(brackets);
		 //arrayString.substring(1, 5);
		
		//System.out.println(arrayString.substring(1, 14));
		//System.out.println(Arrays.toString(brackets).charAt(4));
		//System.out.println(Arrays.toString(brackets).charAt(7));
		//System.out.println(Arrays.toString(brackets).charAt(10));
		//System.out.println(Arrays.toString(brackets).charAt(13));
		
		
		//String wordSet = "orahp";
		//String attempt = "pharo";
		//System.out.println(wordSet.contains(attempt));
		
		//System.out.println(allLetters("phaaaaaro", "orahaaaap"));
		
		//ArrayList<String> tempPossibleWords = findPossibleWords();
		
		///int[] tempTest = getBrackets(possibleWords,  "aorpyhl", "l");
		//getBrackets(tempSet);
		
		
		//System.out.println(tempTest);
		
		//for (int s : tempTest) {
			//System.out.println(s);
		//}
		
		
		//String randomWordSet = "SHLOMY";
		
		//char tempMiddleLetter = randomWordSet.charAt(3);
		//String middleLetter = Character.toString(tempMiddleLetter);
		
		//System.out.println(randomWordSet.contains("S"));
		
		//String wordSet = "aorpyhl";
		//String attempt = "pharo";
		
		//for(int i = 0; i < attempt.length(); i++) {
			//System.out.println(!(attempt.contains(String.valueOf(wordSet.charAt(i))))); 
		//}
		
		//System.out.println(getMessage(attempt, wordSet, "p")); 


	}
	
	public static ArrayList<String> findPossibleWords(String[] possibleWords, String wordSet, String middleLetter){
		
		ArrayList<String> tempPossibleWords = new ArrayList<String>();

		for(int i = 0; i < possibleWords.length; i++) {
			int counter = 0;
			if(possibleWords[i].contains(middleLetter) && possibleWords[i].length() >= 4) {
				for(int j = 0; j < possibleWords[i].length(); j++) {
					if(wordSet.contains(String.valueOf(possibleWords[i].charAt(j)))) {
						counter++;
						//System.out.println(counter);
					}
				}
			}
			
			if(counter == possibleWords[i].length()) {
				tempPossibleWords.add(possibleWords[i]);
			}
		}

		return tempPossibleWords;
	}
	
	public static String[] createWordsFromFile(String path) throws IOException {
		Path p = Paths.get(path);
		List<String> lines = Files.readAllLines(p);
		String[] returnArray = new String[lines.size()];
		returnArray = lines.toArray(returnArray);
		return returnArray;
	}
	
	public static int[] getBrackets(ArrayList<String> tempPossibleWords) { 
		int totalPoints = 0;
		//ArrayList<String> tempPossibleWords = findPossibleWords();
		for (int i = 0; i < tempPossibleWords.size(); i++) {
			if(allLetters(tempPossibleWords.get(i), tempPossibleWords.toString())) {
				totalPoints += (tempPossibleWords.get(i).length() + 7);
			}else if(tempPossibleWords.get(i).length() == 4){
				totalPoints += 1;
			}else {
				totalPoints += tempPossibleWords.get(i).length();
			}
		}
		
		int[] returnArray = new int[5];
			returnArray[0] = (int)(totalPoints * 0.25);
			returnArray[1] = (int)(totalPoints * 0.50);
			returnArray[2] = (int)(totalPoints * 0.75);
			returnArray[3] = (int)(totalPoints * 0.90);
			returnArray[4] = (totalPoints * 1);
		
		return returnArray;
	}
	
	public static String getMessage(String attempt, String wordSet, String middleLetter) {
		String returnString = "";
		
		for(int i = 0; i < attempt.length(); i++) {
			if(!(attempt.contains(String.valueOf(wordSet.charAt(i))))) {
				returnString = "User Entered A Unsupported Letter";
			}
		}
		if(!(attempt.contains(middleLetter))) {
			returnString = "Missing Middle Letter";
		}else if(attempt.length() < 4) {
			returnString = "Word Entered is Too Short";
		}else if(attempt.length() == 4) {
			returnString = "Good!";
		}else if(attempt.length() == 5) {
			returnString = "Great!";
		}else if(attempt.length() == 6) {
			returnString = "Excellent!";
		}else if(attempt.length() >= 7) {
			returnString = "Legendary!";
		}
		return returnString;
	}
	
	public static boolean allLetters(String word, String wordSet) {
		String[] setLetters = wordSet.split("");
		for(String c : setLetters) {
			if(!(word.contains(c))) {
				return false;
			}
		}
		return true;
	}
}








